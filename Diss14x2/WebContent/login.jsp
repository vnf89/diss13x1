<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">

    <title>Log in</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script src="js/jquery-1.9.1.js"></script>
	<script src="js/jquery-ui-1.10.1.custom.js"></script>
	<script src="js/custom.js"></script>
	
  </head>

  <body onload="initialize()">
  
    <%

	if ((String)session.getAttribute("user")!=null) {
		String redirectURL = "home.jsp";
		response.sendRedirect(redirectURL);
	}
	%>

<!--  MENU -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="home.jsp">Digital Publisher</a>
          <div class="nav-collapse collapse">

            <ul class="nav">
              <li class="active"><a href="home.jsp">Home</a></li>
				<% 
					if (session.getAttribute("user")==null) {%>
						              <li><a href="login.jsp">Login</a></li>
						              <li><a href="signUp.jsp">Sign up</a></li>
						
					<%}else{ 
							%>
  								<li class="dropdown">
    								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Journals<b class="caret"></b></a>
    								<ul class="dropdown-menu">
     									<li><a href="addMagazine.jsp">Add</a></li>
  										<li><a href="reviewer.jsp">Review</a></li>
    								</ul>
  								</li>								
							<%%>
  										<li><a href="reviewer.jsp">Review</a></li>
							<%%>
  								<li class="dropdown">
    								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Journals<b class="caret"></b></a>
    								<ul class="dropdown-menu">
										<li><a href="published.jsp">Published</a></li>
										<li><a href="unpublished.jsp">Unpublished</a></li>
    								</ul>
  								</li>								
								
								
															
							<%%>
					              
					              <li><a href="messagesList.jsp">Messages</a></li>
					              <li><a href="objectives.jsp">Aims & Objectives</a></li>
					              <li><a href="logOut.jsp">Logout</a></li>
					
					<%} 
				%>              
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
    
<!-- CONTAINER -->
    <div class="container">

      <form class="form-signin" role="form" method="post" action="SignIn">
        <h2 class="form-signin-heading">Please log in</h2>
        
            <div class="control-group">

          <!-- Text input-->
          <label class="control-label" for="input01">User name</label>
          <div class="controls">
            <!-- <input id="title" type="text" placeholder="Article Title" class="input-xlarge"> -->
            <input name="username" id="username" type="text" value="${fn:escapeXml(param.user)}" class="input-xlarge" placeholder="User name" required autofocus>
          </div>
          
          <label class="control-label" for="input02">Password</label>
          <div class="controls">
			<input name="password" id="password" type="password" value="${fn:escapeXml(param.password)}" class="input-xlarge" placeholder="Password" required>
          </div>
          
        </div>
        
        
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
        <button class="btn btn-primary" type="submit" id="loginBtn">Sign in</button>
        <p><span id="errorDiv" name="errorDiv" class="errorDiv" ></span></p>
      </form>

	<div id="results" name ="results"></div>
	
    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>