var beanName='';
var beanId='';
var msgid='';
var artid='';

function setBean(b){
	beanName=b;
}


function getBean(){
	return beanName;
}


function setBeanId(bid){
	beanId=bid;
}

function getBeanId(){
	return beanId;
}

function setMsgId(mid){
	msgid=mid;

}

function getMsgId(){
	return msgid;
}

function setArtId(aid){
	artid=aid;
}

function getArtId(){
	return artid;
}

function readMessages(){
	$(document).ready(function(){

		//load messages into messages div
  	$.post('Messages',{user:getBean(),type:'1',uid:getBeanId()},function(responseText) {
			$('#messages').html(responseText);  
  	});
  	
	});
	
}

function readMessagesReader(){
	$(document).ready(function(){
		var id = $('#messageHiddenArt').val();
		var mid = $('#messageHiddenAuthor').val();
		var idSender = $('#messageHiddenSender').val();
		//load messages into messages div
  	$.post('MessageReader',{user:getBean(),type:'1',mid:mid,id:id,idSender:idSender},function(responseText) {
			$('#msgsEditor').html(responseText);  
  	});
  	
	});
	
}

//Accept or Reject a message
function acceptMessage(msgid){
	    	$.post('ApproveMsg',{msgid:msgid},function(responseText) {
	    		$('#messageDetail').html(responseText);
	    	});
}

function rejectMessage(msgid){
	    	$.post('RejectMsg',{msgid:msgid},function(responseText) {
	    		$('#messageDetail').html(responseText);
	    	});
}




function initialize() {
		$(document).ready(function(){
			var formOk=false;
			var searchType=1;
			$('#subscribeForm').hide();
			$('#sendMsgForm').hide();
  			$( "#errorSub" ).hide();
  			$('.datepicker').datepicker();
			$('#dp4').datepicker();
			$('#dp5').datepicker();
  			$( "#alert" ).hide();
  			$( "#key" ).hide();
  			$( "#aut" ).hide();
  			$( "#dat" ).hide();
  			$( "#updtDiv" ).hide();

	      	
	      	//perform this function every 1000ms = 1second
  			setInterval(function() {
  				//if user is currently located at the messages.jsp then get message
  				path=window.location.pathname;
  				if (path.indexOf("messages.jsp") != -1){
  					
  					//if message id is set to null redirect to 
  	 				if (getMsgId()=='null'){
  	  					window.location.href="messagesList.jsp";
  	  				}
  					
  					//
  			      	$.post('Messages',{user:getMsgId(),type:'2',uid:getBeanId()},function(responseText) {
  		      			$('#messages').html(responseText);  
  			      	});
  				}else{
  					//do nothing
  					if(path.indexOf("messageReader.jsp") != -1){
  						var id = $('#messageHiddenArt').val();
  						var mid = $('#msgHiddenAuthor').val();
  						var idSender = $('#msgHiddenID').val();
  							
  							if (idSender=='null'){
  			  	 				//window.location = "./messageReader.jsp?artid=" + artid + "&authorId=" + authorId;
  			  	  				window.location.href="messageReaderList.jsp?id=" +id+"&mid=" + mid;

  			  	  			}
  			  	  			
  							$.post('MessageReader',{user:getBean(),type:'2',mid:mid,id:id, idSender:idSender},function(responseText) {
  		  		      			$('#messageReader').html(responseText);  
  		  			      	});
  					}
  					

  					}
  			}, 1000);
  			
  			
  			path1=window.location.pathname;
  			if(path1.indexOf("unpublished.jsp") != -1){
					$.post('Articles',{type:'1'},function(responseText) {
		      			$('#articles').html(responseText);  
			      	});
			}else if(path1.indexOf("published.jsp") != -1){
					$.post('Articles',{type:'2'},function(responseText) {
		      			$('#articles').html(responseText);  
			      	});
			}else if(path1.indexOf("reviewList.jsp") != -1){
				$.post('ListReviews',{artid:getArtId()},function(responseText) {
	      			$('#listreviews').html(responseText);  
		      	});
			}else if(path1.indexOf("objectives.jsp") != -1){
					//handles objective content
					$.get('Objectives',function(responseText) {
					$("#results").html(responseText); 
					$("#editObjective").val(responseText); 
					});  			
			}
  			
  			
  			$('#rejectVer').click(function() { 
  				
  				var version = $("#version").val();
  				var artid = $("#artid").val();

  				//alert(name+" "+email);
  				//alert(version+" "+artid);
  									
  		      	$.post('RejectVersion',{version:version,artid:artid},function(responseText) {
  		      		$('#reviewOverall').html(responseText);
  		      	});
  				
  			});	
  			
  			$('#publishVer').click(function() { 
  				
  				var version = $("#version").val();
  				var artid = $("#artid").val();

  				//alert(name+" "+email);
  				//alert(version+" "+artid);
  									
  		      	$.post('PublishVersion',{version:version,artid:artid},function(responseText) {
  		      		$('#reviewOverall').html(responseText);
  		      	});
  				
  			});	
  			
  			$('#rejectRev').click(function() { 
  				
  				var revid = $("#revid").val();

  				//alert(name+" "+email);
  				//alert(version+" "+artid);
  				
  		      	$.post('RejectReview',{revid:revid},function(responseText) {
  		      		$('#reviewOverall').html(responseText);
  		      	});
  				
  			});	
  			
  			$('#approveRev').click(function() { 
  				
  				var revid = $("#revid").val();

  				//alert(name+" "+email);
  				//alert(version+" "+artid);
  									
  		      	$.post('ApproveReview',{revid:revid},function(responseText) {
  		      		$('#reviewOverall').html(responseText);
  		      	});
  				
  			});	
  			
  			//handles radio button change in home page
  			$("input[name='searchType']").change(function(){
  				var value=$("input[name='searchType']:checked").val();

  				
  				if (value=='Title'){	//Title radio is selected
  					searchType=1;
  		  			$( "#tit" ).show(1);
  		  			$( "#key" ).hide();
  		  			$( "#aut" ).hide();
  		  			$( "#dat" ).hide();  		  			

  				}else if(value=='Author'){	//Author radio is selected
  					searchType=2;
  		  			$( "#aut" ).show(1);
  		  			$( "#tit" ).hide();
  		  			$( "#key" ).hide();
  		  			$( "#dat" ).hide();
  		  			

  				}else if(value=='Keywords'){	//Keywords radio is selected
  					searchType=3;
  		  			$( "#key" ).show(1);
  		  			$( "#tit" ).hide();
  		  			$( "#aut" ).hide();
  		  			$( "#dat" ).hide();

  				}else{	//Date radio is selected
  					searchType=4;
  		  			$( "#dat" ).show(1);
  		  			$( "#tit" ).hide();
  		  			$( "#key" ).hide();
  		  			$( "#aut" ).hide();

  				}
  				
  				
  				
  			});
  			
			//function to restrict typing more than specified word limit 
			var wordLimit = 250;
			$('textarea#abstractarea').keyup(function() {
			   $this = $(this);

			   var counter = $this.val().split(/\b[\s,\.-:;]*/).length - 1;

			   if (counter < wordLimit) {
			      chars = $this.val().length;
					$('#wordsCount').text(wordLimit-counter + ' words left');

			    }
			   else{
			    var text = $(this).val();
			    var new_text = text.substr(0,chars);
			    $(this).val(new_text);
				$('#wordsCount').text(wordLimit-counter + ' words left');

			   }
			});
			
			//function which counts keywords entered
			$(".keyword").keyup(function(){
				var kwords = $(this).val();
				if(kwords.length > 0){

					var kCount = 0;
					//each contiguous string of space characters is being replaced with the empty string
					var a=kwords.replace(/\s+/g,' ');
					a=a.split(' ');
					for (z=0; z<a.length; z++) {
						if (a[z].length > 0) 
							kCount++;
					}
					
					
					$(".keywordsCount").html(kCount);
					
				}else{
					$(".keywordsCount").html('');

				}

			});
			
			
			//handles search form submission
			$('#submitBtn').click(function() { 
				
				var title = $("#title").val();				
				var author = $("#author").val();
				var keyword = $("#keyword").val();
				var startDate = $("#startDate").val();
				var endDate = $("#endDate").val();


				if (title==''&&author==''&&keyword==''&&startDate==''&&endDate==''){
	      			$( "#alert" ).show();
	      			$('#alert').show().find('strong').text('Error. Please complete at least one field.');
	      			formOk=false;	      			

				}else{
					
					
					if (searchType==1){ // TITLE
						if(title==''){
			      			formOk=false;	      			
			      			$( "#alert" ).show();
			      			$('#alert').show().find('strong').text('Error. Please enter a title.');
						}else{
			      			$( "#alert" ).show();
			      			formOk=true;	      			
						}
					}else if(searchType==2){	//AUTHOR
						if(author==''){
			      			formOk=false;	      			
			      			$( "#alert" ).show();
			      			$('#alert').show().find('strong').text('Error. Please enter an author.');
						}else{
			      			$( "#alert" ).show();
			      			formOk=true;	      			
						}					
					}else if(searchType==3){	//KEYWORDS
						if(keyword==''){
			      			formOk=false;	
			      			$( "#alert" ).show();
			      			$('#alert').show().find('strong').text('Error. Please enter at least one keyword.');			      			
						}else{
			      			$( "#alert" ).show();
			      			formOk=true;	      			
						}
					}else{
						if(startDate=='' || endDate==''){	//DATE INTERVAL
			      			formOk=false;	      	
			      			$( "#alert" ).show();
			      			$('#alert').show().find('strong').text('Error. Please choose start and end date.');			      			
						}else{
			      			$( "#alert" ).show();
			      			formOk=true;	      			
						}
					}
					
					
					if (formOk==true){
		      			$( "#alert" ).hide();
				      	$.post('search',{title:title,author:author,keyword:keyword,searchType:searchType,startDate:startDate,endDate:endDate},function(responseText) {
			      			$('#results').html(responseText);  
				      	});
					}else{
						
					}

            
            
				}
			});
			
			//handles login form submission
			$('#loginBtn').click(function() { 
				var user = $("#username").val();				
				var pass = $("#password").val();
				var receivedtext='';
				
				if (user=='' || pass==''){
	      			$( "#errorDiv" ).show();
					$('#errorDiv').html('<font color=red>Please complete both fields.</font>');

				}else{
	      			$( "#errorDiv" ).hide();


			      	$.post('login',{user:user,pass:pass},function(responseText) {
			      		
			      		var receivedtext = new String(responseText);

			      		if (receivedtext.substring(0,4)=='succ'){
			      			window.location.replace("home.jsp");
			      			}else{
				      			$( "#errorDiv" ).show();
								$('#errorDiv').html('<font color=red>Wrong username or password.</font>');
			      			}
			      		

			      		
			      	});
            
            
				}

			});
			

			//handles subscribe button action
			$('#subscribe').click(function() { 
				
				var name = $("#name").val();				
				var email = $("#email").val();
				var version = $("#version").val();
				var artid = $("#artid").val();

				//alert(name+" "+email);
				//alert(version+" "+artid);
									
				if (name && email){
	      			$( "#errorSub" ).hide();
	      			alert('both ok');
				}else{
					alert('both NOT ok');
	      			$( "#errorSub" ).show();
					$('#errorSub').html('<font color=red>Please enter a message.</font>');
				}
	
			});
			
		
			$('.showFormBtn').click(function() { 
				$("#name").val('');				
				$("#email").val('');		
				$('#subscribeForm').show(1000);

			});
	
			
			$('.cancel').click(function() { 
				$("#name").val('');				
				$("#email").val('');	
				$('#subscribeForm').hide(1000);

			});
			
			
			
			//Send message regarding article
			$('#sendMsg').click(function() { 
				var authorid = $("#msgID").val();				
				var msgField = $("#messageField").val();
				var aaid = $("#aaid").val();

			
				if (msgField){
	      			$( "#errorMsg" ).hide();

				$.post('Messages',{user:authorid,type:'3',uid:getBean(),msg:msgField,aaid:aaid},function(responseText) {
					$("#messageField").val('');				
					$('#sendMsgForm').hide(1000);
	      			$( "#errorMsg" ).hide();

			      	});
				}else{
	      			$( "#errorMsg" ).show();
					$('#errorMsg').html('<font color=red>Please enter a message.</font>');
				}
			});

			//handles messages Author-Reader for Editor
			$('#showMsgEditor').click(function() { 
				//window.location = "./messageReader.jsp?artid=" + artid + "&authorId=" + authorId;
				var id = $('#showMessageArtId').val();
				var mid = $('#showMessageAuthorId').val();
				window.location = "./messageReaderList.jsp?id=" + id + "&mid=" + mid;
			});	
			
			
			$('.showMsgBtn').click(function() { 
				$("#messageField").val('');				
				$('#sendMsgForm').show(1000);

			});
			
			$('.cancelMsg').click(function() { 
				$("#messageField").val('');				
				$('#sendMsgForm').hide(1000);

			});
			
			
			$('.showUpdtBtn').click(function() { 
				$("#upFile").val('');		
				$('#updtDiv').show(1000);
			});	
			
			$('.cancelUpdt').click(function() { 
				$("#upFile").val('');		
				$('#updtDiv').hide(1000);
			});

			
			
			$('#sendMessage').click(function() { 
				var message = $("#msg").val();	
				if (message){
					$.post('Messages',{user:getMsgId(),type:'3',uid:getBeanId(),msg:message},function(responseText) {
						$("#msg").val('');				
  			      	});
				}else{
					
				}

			});
			
			$("#msg").keyup(function(event){
				
			    if(event.keyCode == 13){
			    	if ($('#chkbox').is(':checked')) {
						var message = $("#msg").val();	
						if (message){
							$.post('Message',{user:getMsgId(),type:'3',uid:getBeanId(),msg:message},function(responseText) {
								$("#msg").val('');				

		  			      	});	
						}else{
												
						}
			    	}else{
			    		
			    	}

		    
					}
			});
			
			
			//DATEPICKER SETTINGS
			//SOURCE : http://www.eyecon.ro/bootstrap-datepicker/
			var startDate = new Date(2013,01,01);
			var endDate = new Date(2013,01,01);
			$('#dp4').datepicker().on('changeDate', function(ev){

				if (ev.date.valueOf() > endDate.valueOf()){
					$('#alert').show().find('strong').text('Start date cannot be after end date.');
					formOk=false;					
				} else {
					$('#alert').hide();
					startDate = new Date(ev.date);
					$('#startDate').text($('#dp4').data('date'));
					formOk=true;
				}
				$('#dp4').datepicker('hide');
			});
		$('#dp5').datepicker()
			.on('changeDate', function(ev){
				if (ev.date.valueOf() < startDate.valueOf()){
					$('#alert').show().find('strong').text('End date cannot be before the start date.');
					formOk=false;					
				} else {
					$('#alert').hide();
					endDate = new Date(ev.date);
					$('#endDate').text($('#dp5').data('date'));
					formOk=true;					
				}
				$('#dp5').datepicker('hide');
			});

		});	//END OF DATEPICKER SETTINGS
		
		
	}

	function check_file(){

    str=document.getElementById('upFile').value.toUpperCase();
    suffix=".PDF";
    suffix2=".pdf";
    if(!(str.indexOf(suffix, str.length - suffix.length) !== -1||
                   str.indexOf(suffix2, str.length - suffix2.length) !== -1)){
        document.getElementById('upFile').value='';
        document.getElementById('errorFileType').innerHTML ='<font color="red">File type not allowed. Allowed files: *.pdf</font>';
    }
	}

	
	function check_updt(){
		var fileBool=false;
		
		if ($('#upFile').val().toUpperCase()==''){
			$('#upFile').closest('.control-group').removeClass('success').addClass('error');
			$('#upFile').next('.help-block').show();				
			$('#upFile').next('.help-block').text('Please select a file.');
			$('#errorFileType').html ='';				
			fileBool = false;						
		}else{
			$('#upFile').closest('.control-group').removeClass('error').addClass('success');
			$('#upFile').next('.help-block').hide(500);	
			$('#errorFileType').html ='<font color="#d14">No files selected.</font>';
			
			fileBool = true;				
			
		}
		
		if (fileBool){
			return true;
		}else{
			return false;
		}
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function check_reg(){
		
		var nameBool=false;
		var surnameBool=false;
		var emailBool=false;
		var emailRepeatBool=false;
		var passwordBool=false;
		var passwordRepeatBool=false;
		
		$(document).ready(function(){
			if ($('#name').val()==''){
				$('#name').closest('.control-group').removeClass('success').addClass('error');
				$('#name').next('.help-block').show();				
				$('#name').next('.help-block').text('Please enter other authors.');	
				nameBool = false;
			}else{
				$('#name').closest('.control-group').removeClass('error').addClass('success');
				$('#name').next('.help-block').hide(500);				
				nameBool = false;

			}
			
			if ($('#surname').val()==''){
				$('#surname').closest('.control-group').removeClass('success').addClass('error');
				$('#surname').next('.help-block').show();				
				$('#surname').next('.help-block').text('Please enter other authors emails.');	
				surnameBool = false;						
			}else{
				$('#surname').closest('.control-group').removeClass('error').addClass('success');
				$('#surname').next('.help-block').hide(500);				
				surnameBool = true;

			}
			
			if ($('#email').val()==''){
				$('#email').closest('.control-group').removeClass('success').addClass('error');
				$('#email').next('.help-block').show();				
				$('#email').next('.help-block').text('Please enter a title.');	
				emailBool = false;
				   
			}else{
				$('#email').closest('.control-group').removeClass('error').addClass('success');
				$('#email').next('.help-block').hide(500);				
				emailBool = true;

			}
			
			if ($('#emailRepeat').val()==''){
				$('#emailRepeat').closest('.control-group').removeClass('success').addClass('error');
				$('#emailRepeat').next('.help-block').show();				
				$('#emailRepeat').next('.help-block').text('Please enter abstract for the article.');	
				emailRepeatBool = false;				
			}else{
				if ($('#emailRepeat').val()==$('#email').val()){
					$('#emailRepeat').closest('.control-group').removeClass('error').addClass('success');
					$('#emailRepeat').next('.help-block').hide(500);				
					emailRepeatBool = true;
				}else{
					$('#emailRepeat').closest('.control-group').removeClass('success').addClass('error');
					$('#emailRepeat').next('.help-block').show();				
					$('#emailRepeat').next('.help-block').text('Emails do not match.');	
					emailRepeatBool = false;						

				}		

			}
			
			if ($('#pass').val()==''){
				$('#pass').closest('.control-group').removeClass('success').addClass('error');
				$('#pass').next('.help-block').show();				
				$('#pass').next('.help-block').text('Please enter keywords for the article.');	
				passwordBool = false;						
			}else{
				$('#pass').closest('.control-group').removeClass('error').addClass('success');
				$('#pass').next('.help-block').hide(500);				
				passwordBool = true;						

			}			
			
			
			if ($('#passRepeat').val().toUpperCase()==''){
				$('#passRepeat').closest('.control-group').removeClass('success').addClass('error');
				$('#passRepeat').next('.help-block').show();				
				$('#passRepeat').next('.help-block').text('Please enter keywords for the article.');				
				passwordRepeatBool = false;						
			}else{
				
				if ($('#passRepeat').val()==$('#pass').val()){
					$('#passRepeat').closest('.control-group').removeClass('error').addClass('success');
					$('#passRepeat').next('.help-block').hide(500);				
					passwordRepeatBool = false;
				}else{
					$('#passRepeat').closest('.control-group').removeClass('success').addClass('error');
					$('#passRepeat').next('.help-block').show();				
					$('#passRepeat').next('.help-block').text('Passwords do not match.');	
					passwordRepeatBool = false;						

				}
				
	
			}	
			

			
			

			
		});
		
		if (nameBool && surnameBool && emailBool && emailRepeatBool && passwordBool && passwordRepeatBool){
			alert('true');
			return false;
		}else{
			alert('false');

			return false;			
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function check_val(){
		
		var authorsBool,emailsBool,titleBool,absBool,keyBool,fileBool=false;
		
		$(document).ready(function(){
			if ($('#authors').val()==''){
				$('#authors').closest('.control-group').removeClass('success').addClass('error');
				$('#authors').next('.help-block').show();				
				$('#authors').next('.help-block').text('Please enter other authors.');	
				authorsBool = false;
			}else{
				$('#authors').closest('.control-group').removeClass('error').addClass('success');
				$('#authors').next('.help-block').hide(500);				
				authorsBool = true;

			}
			
			if ($('#authorsemails').val()==''){
				$('#authorsemails').closest('.control-group').removeClass('success').addClass('error');
				$('#authorsemails').next('.help-block').show();				
				$('#authorsemails').next('.help-block').text('Please enter other authors emails.');	
				emailsBool = false;						
			}else{
				$('#authorsemails').closest('.control-group').removeClass('error').addClass('success');
				$('#authorsemails').next('.help-block').hide(500);				
				emailsBool = true;

			}
			
			if ($('#title').val()==''){
				$('#title').closest('.control-group').removeClass('success').addClass('error');
				$('#title').next('.help-block').show();				
				$('#title').next('.help-block').text('Please enter a title.');	
				titleBool = false;
				   
			}else{
				$('#title').closest('.control-group').removeClass('error').addClass('success');
				$('#title').next('.help-block').hide(500);				
				titleBool = true;

			}
			
			if ($('#abstractarea').val()==''){
				$('#abstractarea').closest('.control-group').removeClass('success').addClass('error');
				$('#abstractarea').next('.help-block').show();				
				$('#abstractarea').next('.help-block').text('Please enter abstract for the article.');	
				absBool = false;				
			}else{
				$('#abstractarea').closest('.control-group').removeClass('error').addClass('success');
				$('#abstractarea').next('.help-block').hide(500);				
				absBool = true;				

			}
			
			if ($('#keyword').val()==''){
				$('#keyword').closest('.control-group').removeClass('success').addClass('error');
				$('#keyword').next('.help-block').show();				
				$('#keyword').next('.help-block').text('Please enter keywords for the article.');	
				keyBool = false;						
			}else{
				$('#keyword').closest('.control-group').removeClass('error').addClass('success');
				$('#keyword').next('.help-block').hide(500);				
				keyBool = true;						

			}			
			
			
			if ($('#upFile').val().toUpperCase()==''){
				$('#upFile').closest('.control-group').removeClass('success').addClass('error');
				$('#upFile').next('.help-block').show();				
				$('#upFile').next('.help-block').text('Please select a file.');
				$('#errorFileType').html ='';				
				fileBool = false;						
			}else{
				$('#upFile').closest('.control-group').removeClass('error').addClass('success');
				$('#upFile').next('.help-block').hide(500);	
				$('#errorFileType').html ='<font color="#d14">No files selected.</font>';
				
				fileBool = true;				
				
			}	
			

			
			

			
		});
		
		if (fileBool && absBool && keyBool && titleBool && emailsBool && authorsBool){
			return true;
		}else{
			return false;			
		}

	}
	
	function keywordAdd(keyid,usid){
		
		$('document').ready(function(){

			if (usid=='null'){
				alert(usid);
	      		$( "#errorSubKey" ).show(1000);
				$('#errorSubKey').html('<font color=red>Please login to subscribe.</font>');
			
			}else{
			var keyBtn = $("body").find('#'+keyid);
			$.post('Subscribe',{keyid:keyid,usid:usid,type:'2'},function(responseText) {
		      		
		      		var receivedtext = new String(responseText);

		      		if (receivedtext.substring(0,3)=='yes'){
			      		$( "#errorSubKey" ).show(1000);
						$('#errorSubKey').html('<font color=green>You have succesfully subscribed for this keyword.</font>');
		      			keyBtn.attr("disabled", true);
		      		}else{
			      		$( "#errorSubKey" ).show(1000);
						$('#errorSubKey').html('<font color=red>You have already subscribed for this keyword.</font>');
						keyBtn.attr("disabled", true);						
		      		}

		      	});
			}
		});
		
	}
	
	
	function subscribe(id){
		alert("id is"+id);
	}

	function displayArticle(x){
		//alert("id is" + x);
		window.location.href="View?artid="+x;
	}
	
	function messageDetail(x){
		//alert("id is" + x);
		window.location.href="messageDetail.jsp?msgid="+x;
	}
	
	function displayMessage(x){
		//alert("id is" + x);
		window.location.href="messages.jsp?msgid="+x;
	}
	

	function readAvailableArticles(){
		$(document).ready(function(){
			
		 $.post('Reviewer',{type:'1'},function(responseText) {
				$('#selectedArticles').html(responseText);  
				
			  	$.post('Reviewer',{type:'2'},function(responseText) {
					$('#availableArticles').html(responseText);  
					
				  	$.post('Reviewer',{type:'3'},function(responseText) {
						$('#articleCorrections').html(responseText);  
						
					  	$.post('Reviewer',{type:'4'},function(responseText) {
							$('#correctionsArticles').html(responseText); 
					  	});
				  	});
		  	});
				
		 });	

		});
		
	}


	function displayArticleReviewer(x,type,y){
		if (type=='1'){
			window.location.href="ReviewForm?artid="+x;
		}else if(type=='2'){
			window.location.href="AbstractView?artid="+x;
		}else {
			window.location.href="ReviewCorrect?artid="+x+"&reviewid="+y;
		}
	}
	
	function checkReview(){
		var overallBool,summaryBool,criticBool,otherBool=false;
		
		if ($('#overallJudgement').val().toUpperCase()==''){
			$('#overallJudgement').closest('.control-group').removeClass('success').addClass('error');
			$('#overallJudgement').next('.help-block').show();				
			$('#overallJudgement').next('.help-block').text('Please fill overall information.');
			$('#errorFileType').html ='';				
			overallBool = false;						
		}else{
			$('#overallJudgement').closest('.control-group').removeClass('error').addClass('success');
			$('#overallJudgement').next('.help-block').hide(500);	
			$('#errorFileType').html ='<font color="#d14"></font>';
			
			overallBool = true;				
			
		}
		
		if ($('#summary').val().toUpperCase()==''){
			$('#summary').closest('.control-group').removeClass('success').addClass('error');
			$('#summary').next('.help-block').show();				
			$('#summary').next('.help-block').text('Please fill summary information.');
			$('#errorFileType').html ='';				
			summaryBool = false;						
		}else{
			$('#summary').closest('.control-group').removeClass('error').addClass('success');
			$('#summary').next('.help-block').hide(500);	
			$('#errorFileType').html ='<font color="#d14">.</font>';
			
			summaryBool = true;				
			
		}
		if ($('#criticism').val().toUpperCase()==''){
			$('#criticism').closest('.control-group').removeClass('success').addClass('error');
			$('#criticism').next('.help-block').show();				
			$('#criticism').next('.help-block').text('Please fill Structure criticism of any bad points information.');
			$('#errorFileType').html ='';				
			criticBool = false;						
		}else{
			$('#criticism').closest('.control-group').removeClass('error').addClass('success');
			$('#criticism').next('.help-block').hide(500);	
			$('#errorFileType').html ='<font color="#d14">.</font>';
			
			criticBool = true;				
			
		}
		if ($('#other').val().toUpperCase()==''){
			$('#other').closest('.control-group').removeClass('success').addClass('error');
			$('#other').next('.help-block').show();				
			$('#other').next('.help-block').text('Please fill Other errors information.');
			$('#errorFileType').html ='';				
			otherBool = false;						
		}else{
			$('#other').closest('.control-group').removeClass('error').addClass('success');
			$('#other').next('.help-block').hide(500);	
			$('#errorFileType').html ='<font color="#d14">.</font>';
			
			otherBool = true;				
			
		}
		
		
		
		if (overallBool&summaryBool&otherBool&criticBool){
			return true;
		}else{
			return false;			
		}
		
		
	}
	
	
	//asdawda
	function displayArticleEd(x){
		//alert("id is" + x);
		window.location.href="ViewEd?artid="+x;
	}
	
	function displayReviews(x){
		//alert("id is" + x);
		window.location.href="reviewList.jsp?artid="+x;
	}
	
	function displayDetailReview(x){
		//alert("id is" + x);
		window.location.href="ViewReview?revid="+x;
	}
	
	function displayMessageReader(idSender,id,mid){
		window.location.href="messageReader.jsp?idSender="+idSender+"&id="+id+"&mid="+mid;
	}
