<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <title>Digital Publisher | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/custom.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    
    <script src="js/jquery-1.9.1.js"></script>
	<script src="js/jquery-ui-1.10.1.custom.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
	<script src="js/custom.js">
		var user=<%= session.getAttribute("user") %>;
	</script>
	<script src="js/bootstrap-datepicker.js"></script>
    
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>

  </head>
  <body onload="initialize()">

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="home.jsp">Digital Publisher</a>
          <div class="nav-collapse collapse">

            <ul class="nav">
              <li class="active"><a href="home.jsp">Home</a></li>
				<% 
					if (session.getAttribute("user")==null) {%>
						              <li><a href="login.jsp">Login</a></li>
						              <li><a href="signUp.jsp">Sign up</a></li>
						
					<%}
					else{ 
							%>
  								<li class="dropdown">
    								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Magazines<b class="caret"></b></a>
    								<ul class="dropdown-menu">
     									<li><a href="addMagazine.jsp">Add</a></li>
  										<li><a href="reviewer.jsp">Review</a></li>
    								</ul>
  								</li>								

  								<li class="dropdown">
    								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Content<b class="caret"></b></a>
    								<ul class="dropdown-menu">
										<li><a href="published.jsp">Published</a></li>
										<li><a href="unpublished.jsp">Unpublished</a></li>
    								</ul>
  								</li>								

					              
					              <li><a href="messagesList.jsp">Messages</a></li>
					              <li><a href="objectives.jsp">Aims & Objectives</a></li>
					              <li><a href="logOut.jsp">Logout</a></li>
					
					<%} 
				%>              
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">

      <h1>Welcome 
      
      <% if ((String)session.getAttribute("user") != null){%>
       <%= session.getAttribute("user")%>
      	<%}  else{%>
      		Guest
      	<%}
    	  %>
      
      
</h1>      
      
      
    <fieldset>
      <div id="legend" class="">
        <legend class="">Search articles</legend>
      </div>
    

    
	<table width='100%'>
	<tr>
	<td>
		<div class="control-group">
  		<label class="control-label">Search By</label>
  		<div class="controls">
    		<label class="radio">
      		<input type="radio" name="searchType" value="Title" checked="checked">
      		Title
    		</label>
    		<label class="radio">
      		<input type="radio" name="searchType" value="Author">
      		Author
    		</label>
    		<label class="radio">
      		<input type="radio" name="searchType" value="Keywords">
      		Keywords
    		</label>
    		<label class="radio">
      		<input type="radio" name="searchType" value="Date">
      		Date
    		</label>
  		</div>
		</div>
	</td>
	<td>
	<div id="tit">
	
    <div class="control-group">

          <!-- Text input-->
          <label class="control-label" for="input01">Title</label>
          <div class="controls">
            <input id="title" type="text" placeholder="Article Title" class="input-xlarge">
          </div>
        </div>
	</div>
	<div id="aut">
    <div class="control-group">

          <!-- Text input-->
          <label class="control-label" for="input01">Author</label>
          <div class="controls">
            <input id="author" type="text" placeholder="Author" class="input-xlarge">
          </div>
        </div>
	</div>
	<div id="key">
    <div class="control-group">

          <!-- Text input-->
          <label class="control-label" for="input01">Keyword</label>
          <div class="controls">
			<input type ="text" id="keyword" name="keyword" placeholder="Keyword" class="keyword input-xlarge"/>
			<p class="help-block">Enter keywords seperated by space character.</p>
			<p>Number of keywords: <span class="keywordsCount"></span></p>       
			           
          </div>
        </div>
	</div>
	<div id="dat">
	<table>
		<tr>
			<td>
			<div class="control-group">

          		<!-- Date input-->
          		<label class="control-label" for="input01">From</label>
          		<div class="controls">
          		
          		
          				            
		            
			  <div class="input-append date" id="dp4" data-date-format="yyyy-mm-dd" data-date="2013-01-01">
				<input id="startDate" class="startDate input-large" size="16" type="text" value="" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div>	
<!--           		
          		
          			<span id="startDate">2013-01-01</span>
		            <a href="#" class="btn small" id="dp4" data-date-format="yyyy-mm-dd" data-date="2012-02-20"><i class="icon-calendar"></i></a> -->
				</div>
        	</div>      
			</td>
			</tr>
			<tr>
			<td>
            <div class="control-group">

          		<!-- Date input-->
          		<label class="control-label" for="input01">To</label>
          		<div class="controls">

			  <div class="input-append date" id="dp5" data-date-format="yyyy-mm-dd" data-date="2013-01-01">
				<input id="endDate"  class="endDate" size="16" type="text" value="" readonly>
				<span class="add-on"><i class="icon-calendar"></i></span>
			  </div>

<!--           			<span id="endDate">2013-01-01</span>
		            <a href="#" class="btn small" id="dp5" data-date-format="yyyy-mm-dd" data-date="2012-02-25"><i class="icon-calendar"></i></a> -->
				</div>
        	</div> 			
			</td>
		</tr>
	</table>
	</div>
	
	</td>
	</tr>
	

        </table>
        
        
        <span id="errorDiv" name="errorDiv" class="errorDiv" ></span>
        
        
        	<div class="alert alert-error" id="alert">
				<strong>Error</strong>
			</div>

        
 <div class="form-actions">  
            <button type="submit" class="btn btn-primary" id="submitBtn">Search</button>  

          </div> 
    </fieldset>
      

	<div id="results" name ="results"></div>


    </div> <!-- /container -->

  </body>

</html>