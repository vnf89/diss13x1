<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <jsp:useBean id="LoginBean" class="beans.LogInBean" scope="session" />
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Magazine</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/custom.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap.css" rel="stylesheet">    
    <link href="css/bootstrap-fileupload.min.css" rel="stylesheet">
    <link href="css/bootstrap-fileupload.css" rel="stylesheet">
    
    <script src="js/jquery-1.9.1.js"></script>
	<script src="js/jquery-ui-1.10.1.custom.js"></script>
    <script src="js/bootstrap.min.js"></script>    
	<script src="js/custom.js">
		var user=<%= session.getAttribute("user") %>;
	</script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/bootstrap-fileupload.js"></script>
	
</head>
  <body onload="initialize()">

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="home.jsp">Team M</a>
          <div class="nav-collapse collapse">

            <ul class="nav">
              <li><a href="home.jsp">Home</a></li>
				<% 
					if (session.getAttribute("user")==null) {%>
						              <li><a href="login.jsp">Login</a></li>
						
					<%}else{ 
							if (((String)session.getAttribute("type")).equals("Author")){%>
  								<li class="active dropdown">
    								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Journals<b class="caret"></b></a>
    								<ul class="dropdown-menu">
     									<li><a href="addjournal.jsp">Add</a></li>
  										<li><a href="reviewer.jsp">Review</a></li>
    								</ul>
  								</li>								
							<%}else if (((String)session.getAttribute("type")).equals("Reviewer")){%>
  										<li><a href="reviewer.jsp">Review</a></li>
							<%}else if (((String)session.getAttribute("type")).equals("Editor")){%>
  								<li class="dropdown">
    								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Journals<b class="caret"></b></a>
    								<ul class="dropdown-menu">
										<li><a href="published.jsp">Published</a></li>
										<li><a href="unpublished.jsp">Unpublished</a></li>
    								</ul>
  								</li>								
								
								
															
							<%}%>
					              
					              <li><a href="messagesList.jsp">Messages</a></li>
					              <li><a href="objectives.jsp">Aims & Objectives</a></li>
					              <li><a href="logout.jsp">Logout</a></li>
					
					<%} 
				%>              
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">


     
      
<form id="journal-form" action="Upload" method="post" enctype="multipart/form-data" onsubmit="return check_val()">

    <fieldset>
      <div id="legend" class="">
        <legend class="">Add new journal</legend>
      </div>
    

    
	<table>
	
	<tr>
<td>
    <div class="control-group">

          <!-- Text input-->
          <label class="control-label" for="input01">Authors</label>
          <div class="controls">
            <input id="authors" name="authors" type="text" placeholder="Authors" class="input-xlarge">
			<p class="help-block"></p>
          </div>
        </div>
        </td>


<td>
    <div class="control-group">

          <!-- Text input-->
          <label class="control-label" for="input01">Emails</label>
          <div class="controls">
            <input id="authorsemails" name="authorsemails" type="text" placeholder="Emails" class="input-xlarge">
			<p class="help-block"></p>
          </div>
        </div>
        </td>

</tr>

<tr><td>
    <div class="control-group">

          <!-- Text input-->
          <label class="control-label" for="input01">Article Title</label>
          <div class="controls">
            <input id="title" name="title" type="text" placeholder="Title" class="input-xlarge">
			<span class="help-block"></span>
          </div>
        </div>
        </td>
</tr>

<tr><td colspan="2">
    <div class="control-group">

          <!-- Text input-->
          <label class="control-label" for="input01">Abstract</label>
          <div class="controls">
          <textarea class="xlarge" id="abstractarea" name="abstractarea" rows="15" ></textarea><br />
			<p class="help-block"></p>
          <span id="wordsCount">250 words left</span>
          </div>
        </div>
        </td>

</tr>
<tr>
<td>        
	<div class="control-group">
          <!-- Text input-->
          <label class="control-label" for="input01">Keyword</label>
        <div class="controls">
			<input type ="text" id="keyword" name="keyword" placeholder="Keyword" class="keyword input-xlarge"/>
			<p class="help-block"></p>
			<p>Enter keywords seperated by space character.</p>
			<p>Number of keywords: <span class="keywordsCount"></span></p>       
			           
		</div>
    </div>
</td>
</tr>
	<tr>
	<td>
    <div class="control-group">
	<div class="fileupload fileupload-new" data-provides="fileupload">
	
	  <div class="input-append">
	
	    <div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" id="upFile" name="upFile" size="50" accept=".pdf" onchange="check_file()"/></span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
		<p class="help-block"></p>	    
	</div>
	</div>
	

        </div>
</td>
</tr>

        <tr><td><span id="errorFileType" name="errorFileType" class="errorFileType" ></span></td></tr>
        </table>
 <div class="form-actions">  
<input type="submit"  class="btn btn-primary" id="uploadBtn" value="Upload File"/>
<a href="#myModal" data-toggle="modal" class="btn btn-info" role="button">How to</a>
          </div> 
    </fieldset>
      </form>
      
      <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Instructions</h3>
        </div>
        <div class="modal-body">
          <p><img src="img/instructions.png" alt="instructions"/></p>
          
         
          <table class='table' width='100%'>
          <thead>
          <tr>
          	<th>Numbers</th>
          	<th>How to</th>
          	<th>Required</th>
          </tr>
          </thead>
          <tbody>
          <tr>
          	<td>1-2</td>
          	<td>Fill in the names and emails of authors who contributed for this article. Names and emails must be seperated by comma ( , ) character and must have equal number. e.g.
          	Authors: Name1 Surname1, Name2 Surname 2 and Emails: Email1, Email2 etc.</td>
          	<td>Yes</td>
          </tr>
          	<td>3</td>
          	<td>Fill in the title of the article.</td>
          	<td>Yes</td>
          </tr>
          <tr>
          	<td>4</td>
          	<td>Write the abstract of the article within 250 words.</td>
          	<td>Yes</td>
          </tr>
          <tr>
          	<td>5</td>
          	<td>Indicates the words left for abstract.</td>
          	<td>Yes</td>
          </tr>  
          <tr>
          	<td>6</td>
          	<td>Write keywords related to the article seperated by space character e.g. binary graphics tree etc.</td>
          	<td>Yes</td>
          </tr> 
          <tr>
          	<td>7</td>
          	<td>Indicates the number of keywords.</td>
          	<td>Yes</td>
          </tr>
          <tr>
          	<td>8</td>
          	<td>Select the article file you want to upload. File format allowed: .pdf</td>
          	<td>Yes</td>
          </tr>  
          <tr>
          	<td>9</td>
          	<td>Press Upload File button when all is filled in properly.</td>
          	<td>Yes</td>
          </tr>  
          </tbody>
                                                                                         
          </table>
          <p>If the form is not filled in properly then errors will be displayed.</p>
          
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
      </div>
  
  
  
  
    </div> <!-- /container -->

  </body>
</html>