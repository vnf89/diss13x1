<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    
	<title>Sign up</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
</head>
<body>

<!--  MENU -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="home.jsp">Digital Publisher</a>
          <div class="nav-collapse collapse">

            <ul class="nav">
              <li class="active"><a href="home.jsp">Home</a></li>
				<% 
					if (session.getAttribute("user")==null) {%>
						              <li><a href="login.jsp">Login</a></li>
						              <li><a href="signUp.jsp">Sign up</a></li>
						
					<%}else{ 
							if (((String)session.getAttribute("type")).equals("Author")){%>
  								<li class="dropdown">
    								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Journals<b class="caret"></b></a>
    								<ul class="dropdown-menu">
     									<li><a href="addMagazine.jsp">Add</a></li>
  										<li><a href="reviewer.jsp">Review</a></li>
    								</ul>
  								</li>								
							<%}else if (((String)session.getAttribute("type")).equals("Reviewer")){%>
  										<li><a href="reviewer.jsp">Review</a></li>
							<%}else if (((String)session.getAttribute("type")).equals("Editor")){%>
  								<li class="dropdown">
    								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Journals<b class="caret"></b></a>
    								<ul class="dropdown-menu">
										<li><a href="published.jsp">Published</a></li>
										<li><a href="unpublished.jsp">Unpublished</a></li>
    								</ul>
  								</li>								
								
								
															
							<%}%>
					              
					              <li><a href="messagesList.jsp">Messages</a></li>
					              <li><a href="objectives.jsp">Aims & Objectives</a></li>
					              <li><a href="logOut.jsp">Logout</a></li>
					
					<%} 
				%>              
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
    
 <!--  CONTAINER -->   
    <div class="container">

      <form class="form-signin" role="form" method="post" action="SignIn">
        <h2 class="form-signin-heading">Please sign up</h2>
        
            <div class="control-group">

          <!-- Text input-->
          <label class="control-label" for="input01">First name</label>
          <div class="controls">
            <!-- <input name="user" type="text" value="${fn:escapeXml(param.user)}" class="input-xlarge" placeholder="User name" required autofocus> -->
         	<input name="firstName" type="text" value="${fn:escapeXml(param.firstName)}" class="input-xlarge" placeholder="First Name" required autofocus>
          </div>
          
          <label class="control-label" for="input02">Last name</label>
          <div class="controls">
			<input name="lastName" type="text" value="${fn:escapeXml(param.lastName)}" class="input-xlarge" placeholder="Last Name" required autofocus>
          </div>
          
          <label class="control-label" for="input03">E-mail</label>
          <div class="controls">
			<input name="email" type="text" value="${fn:escapeXml(param.email)}" class="input-xlarge" placeholder="E-mail" required autofocus>
          </div>
          
          <label class="control-label" for="input04">Confirm e-mail</label>
          <div class="controls">
			<input name="emailConfirm" type="text" value="${fn:escapeXml(param.emailConfirm)}" class="input-xlarge" placeholder="E-mail" required autofocus>
          </div>
          
          <label class="control-label" for="input05">Password</label>
          <div class="controls">
			<input name="password" type="password" value="${fn:escapeXml(param.password)}" class="input-xlarge" placeholder="Password" required autofocus>
          </div>
          
          <label class="control-label" for="input06">Confirm password</label>
          <div class="controls">
			<input name="passwordConfirm" type="password" value="${fn:escapeXml(param.passwordConfirm)}" class="input-xlarge" placeholder="Password" required autofocus>
          </div>
          
        </div>
        
        

        <button class="btn btn-primary" type="submit" id="submitBtn">Sign in</button>
      </form>

	<div id="results" name ="results"></div>
	
    </div> <!-- /container -->
</body>
</html>