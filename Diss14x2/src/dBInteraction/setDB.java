/**
 * 
 */
/**
 * @author vnavarro89
 *
 */

package dBInteraction;

import java.sql.Connection;
import java.sql.DriverManager;

public class setDB {
	public String dbName = "local_db14x1";
	public String dbUser = "vnavarro";
	public String dbPass = "project14x1";
	public String dbURL = "jdbc:mysql://127.0.0.1:3306/" + dbName;
	
	public String getName() {
		return dbName;
	}

	public void setName(String db) {
		this.dbName = db;
	}

	public String getURL() {
		return dbURL;
	}

	public void setURL(String url) {
		this.dbURL = url;
	}

	public String getUser() {
		return dbUser;
	}

	public void setUser(String u) {
		this.dbUser = u;
	}

	public String getPass() {
		return dbPass;
	}

	public void setPass(String p) {
		this.dbPass = p;
	}

	static Connection conn = null;

	/**
	 * Tests for database connectivity when new details are entered
	 * 
	 * @return true if details are correct
	 */
	public boolean testDB() {
		try {

			Class.forName("com.mysql.jdbc.Driver");

			conn = DriverManager.getConnection(getURL(), getUser(), getPass());
			System.out.println("Database is working ");
			return true;
		} catch (Exception e) {
			System.out.println("Database is not working");
			return false;

		} finally {
			if (conn != null) {
				try {
					conn.close();

				} catch (Exception e) {
				}
			}
		}
	}
	
	public setDB getDBinfo(){
		return this;
	}
	
	
	
	/*public void connectDb(){
		try{

			System.out.println(getURL()+" "+getUser()+" "+getPass());

			Class.forName("com.mysql.jdbc.Driver");


			conn = DriverManager.getConnection (getURL(), getUser(), getPass());
			System.out.println(conn.getMetaData());
			System.out.println ("Database connection established");
		}catch (Exception e) {
			
			System.err.println (e.getMessage()+" Cannot connect to database server");
		}finally {
			if (conn != null) {
				try {
					conn.close ();
					System.out.println ("Database connection terminated");
				}catch (Exception e) {  ignore close errors  }
			} 
		}
	}*/
}
