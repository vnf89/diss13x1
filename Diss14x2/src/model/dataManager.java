/**
 * 
 */
/**
 * @author vnavarro89
 *
 */

package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import beans.LogInBean;
import dBInteraction.setDB;

public class dataManager {
	private LogInBean lb;

	public String dbName;
	public String dbUser;
	public String dbPass;
	public String dbURL;
	static Connection conn = null;
	public ResultSet rs;
	public setDB db;
	public String query = "";
	public Statement statement;
	
	//private File file ;
	
	public dataManager(){
		db = new setDB();
		this.dbName = db.getName();
		this.dbUser = db.getUser();
		this.dbURL = db.getURL();
		this.dbPass = db.getPass();
	}
	
	/**
	 * LogIn . Searches database and if user exists sets mail and password
	 * @param userName
	 * @param password
	 * @return
	 */
	
	public LogInBean getUserData(String user, String password) {
		
		establishConnection();
		
		try {
			/*
				SELECT * FROM Users
				WHERE userName =  'name'
				AND userPassword =  'password'
			 */
			query = "SELECT * FROM Users where userName='" + user + "' AND userPassword='" + password + "'";
			//System.out.println(query);
			rs = statement.executeQuery(query);

			while (rs.next()) {
				// create logIn
				lb = new LogInBean();
				//set mail and password
				//lb.setMail(user);
				lb.setMail(user);
				lb.setPassword(password);
				//lb.setName(rs.getString(2));
				//lb.setId(rs.getString(1));
				//lb.setTypel(rs.getString(6));
			}
			
			System.out.println("We did it!");

		} catch (Exception e) {
			closeConnection();
		}
		closeConnection();

		return lb;
	}
	
	/**
	 * Search for results in database
	 * @param s 
	 * @param title
	 * @param autor
	 * @param keyword
	 * @return
	 */
	public String searchDB(String q, int s) {
		establishConnection();
		String res="";
		try {

			query = q;
			
			PreparedStatement stmt = conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			
			rs = stmt.executeQuery();
			
			while (rs.next()) {

		        
				//if results exist then create a string and return it to servlet 
				res+="<tr onclick='displayArticle("+rs.getString(1)+")'><td>"+rs.getString(2)+"</td><td>"+rs.getString(9)+"</td><td>"+rs.getString(3).substring(0, 4)+"</td></tr>";
			}
			
			

		} catch (Exception e) {
			//close connection
			closeConnection();
			System.out.println(e.getMessage());
		}
		//close connection
		closeConnection();
		
		//return resulting string
		return res;
	}
	
	/**
	 * Establish connection to database
	 */
	public void establishConnection() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(dbURL, dbUser, dbPass);
			statement = conn.createStatement();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * Close database connection.
	 */
	public void closeConnection() {

		try {
			// close all the connections.
			rs.close();
			statement.close();
			conn.close();
		} catch (Exception e) {

			System.out.println(e.getMessage());
		}
	}
}
