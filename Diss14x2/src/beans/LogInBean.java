/**
 * 
 */
/**
 * @author vnavarro89
 *
 */
package beans;

public class LogInBean {
	private String name="";
	private String password="";
	private String email="";
	private String id="";
	private String type="";

	public LogInBean() {

	}

	public String getName() {
		return name;
	}

	public void setName(String n) {
		name = n;
	}	
	
	public String getId() {
		return id;
	}

	public void setId(String i) {
		id = i;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String p) {
		password = p;
	}

	public String getEmail() {
		return email;
	}

	public void setMail(String e) {
		email = e;
	}
	
	public String getType() {
		return type;
	}

	public void setTypel(String t) {
		type = t;
	}
}
