/**
 * 
 */
/**
 * @author vnavarro89
 *
 */

package diss;

//import RequestDispatcher;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dataManager;
import beans.LogInBean;

/**
 * Servlet implementation class SignIn
 */
@WebServlet("/SignIn")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public LogInBean lg = new LogInBean();
	private dataManager dManager;
	private boolean resultsOk = false;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		try {
			// get parameters from form
			String user = request.getParameter("username");
			String password = request.getParameter("password");
				
			//create new login bean and datamanager
			lg = new LogInBean();
			dManager = new dataManager();

			//get results from database
			lg = dManager.getUserData(user, password);

			try {
				//if results are returned 
				if (!lg.getEmail().equals(null))
					resultsOk = true;

			} catch (NullPointerException e) {
				System.out.println("Error on DB return");
				resultsOk = false;
			}

			//create session
			HttpSession session = request.getSession(true);

			//if results are retrieved successfully from database
			if (resultsOk) {
				//set session attributes
				request.getSession().setAttribute("user", lg.getName());
				request.getSession().setAttribute("uid", lg.getId());
				request.getSession().setAttribute("type", lg.getType());
				
				//set login bean as attribute in session
				session.setAttribute("LogInBean", lg);

				out.println("success1");
				out.close();
				
			}else{
				out.println("error");
				out.close();
			}


		} catch (Exception e) {
			System.out.println(e.getMessage());
			out.println("success2");
			out.close();
		}
		
		// forward to login page
		//RequestDispatcher r = request.getRequestDispatcher("hme.jsp");
		//r.forward(request, response);
		
	}

}
